package com.zuitt.wdc044activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController

public class Wdc044ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044ActivityApplication.class, args);
	}

	@GetMapping("/hi")

	public String hi(@RequestParam(value = "name", defaultValue = "user") String name) {
		return  String.format("hi %s!", name);
	}

	@GetMapping("/nameage")

	public String nameage(@RequestParam(value = "name", defaultValue = "Hello") String name, @RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Hello %s! Your age is %d.", name, age);
	}

}
